﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AddHoleAtClick : MonoBehaviour {
    public Text sizeText;
    public GameObject particles;
    float size = 0.03f;
    void Update ()
    {
        
        if (Input.GetKeyDown(KeyCode.Alpha1))
            size -= 2e-3f;
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            size += 2e-3f;
        sizeText.text = (size*100).ToString("0.0");
        if (Input.GetMouseButton(0))
        {
            Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
            RaycastHit[] hits = Physics.RaycastAll(ray);
            foreach (RaycastHit hit in hits)
            {
                STBHManager.manager.AddHole(hit, size);
                //GameObject.Instantiate(particles, hit.point, Quaternion.LookRotation(-hit.normal,Vector3.up));
            }
        }
    }
}
