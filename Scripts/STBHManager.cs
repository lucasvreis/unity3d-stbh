﻿/*
 * TODO
 *   BORA VELHO REVOLUCAO REVOLUCAO REVOLUCAO INSTITUIR O STBHMATERIAL NESSA DROGA heil linus longa vida ao biu gates
*/

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class STBHManager : MonoBehaviour
{
    #region singleton implementation
    private static STBHManager s_Instance = null;

    public static STBHManager manager
    {
        get
        {
            if (s_Instance == null)
            {
                //  FindObjectOfType(...) returns the first ShaderHoleManager object in the scene.
                s_Instance = FindObjectOfType(typeof(STBHManager)) as STBHManager;
            }
            // If it is still null, create a new instance
            if (s_Instance == null)
            {
                GameObject obj = new GameObject("STBH Manager");
                s_Instance = obj.AddComponent(typeof(STBHManager)) as STBHManager;
                Debug.Log("Could not locate an STBH Manager object. STBH Manager was generated automaticly.");
            }

            return s_Instance;
        }
    }
    void OnApplicationQuit()
    {
        s_Instance = null;
    }
    #endregion
    // Maximum number of holes, NEEDS to be set it in the shader too
    [HideInInspector]
    public const int maxHoles = 16;

    private Dictionary<Collider,STBHMaterial> matsInUse;

    void Start()
    {
        // Inicia a co-rotina de verificar a lifetime dos buracos
        //StartCoroutine(UpdateLifetime());
    }

    #region public functions
    /// <summary>
    /// Adds a hole in the specified position, size and material. Position/size in global coordinates.
    /// </summary>
    public void AddHole(RaycastHit hit, float size)
    {
        if(matsInUse.ContainsKey(hit.collider))
        {
            var material = matsInUse[hit.collider];

        }
        else
        {
            var material = hit.collider.GetComponent<STBHMaterial>();
            matsInUse.Add(hit.collider, material);
        }

        //foreach (Material mat in go.GetComponent<Renderer>().sharedMaterials)
        //{
        //    if (!settings.mat_holes.ContainsKey(mat))
        //    {
        //        if (!mat.shader.name.Contains("STBH"))
        //            continue;

        //        bool global = Array.IndexOf(mat.shaderKeywords, "GLOBAL_SPACE") != -1;
        //        settings.mat_holes.Add(mat, new Holes(mat, go.GetComponent<Renderer>(), global));
        //    }
        //    else if (HoleExists(pos, settings.mat_holes[mat]))
        //        return;

        //    Holes currHole = settings.mat_holes[mat];
        //    // Apply transformation for local-space objects
        //    if (!currHole.global)
        //    {
        //        pos = go.transform.InverseTransformPoint(pos);
        //        size /= go.transform.lossyScale.x;
        //    }
        //    // If the holes array is full (se a array dos buracos estiver cheia)
        //    if (currHole.lifetimes.Count == Holes.maxHoles)
        //    {
        //        // Remove the first item to free a slot (remove o primeiro buraco para liberar espaço)
        //        ShaderMoveArrayElements(1, Holes.maxHoles, mat, ref currHole.values);
        //        currHole.lifetimes.RemoveAt(0);
        //    }

        //    // Add hole at the end (adiciona o buraco ao fim da array)
        //    ShaderSetArrayElement(new Vector4(pos.x, pos.y, pos.z, size), currHole.lifetimes.Count, mat, ref currHole.values);
        //    currHole.lifetimes.Add(0);
        //    return;
        //}
    }

    ///<summary>
    ///Clears all holes of a material.
    ///</summary>
    public void ClearMaterial(Material mat)
    {
        //ShaderSetArray(new Vector4[Holes.maxHoles], settings.mat_holes[mat].material);
        //settings.mat_holes.Remove(mat);
    }

    public void ClearAll()
    {
        //foreach(Material mat in settings.mat_holes.Keys)
        //{
        //    ClearMaterial(mat);
        //}
    }
    #endregion

    //private IEnumerator UpdateLifetime()
    //{
    //    while (Application.isPlaying && useLifetime)
    //    {
    //        yield return new WaitForSeconds(0.5f);

    //        foreach (Holes hl in settings.mat_holes.Values)
    //        {
    //            for (int i = 0; i < hl.lifetimes.Count; i++)
    //            {
    //                hl.lifetimes[i] += 1;
    //                if (hl.lifetimes[i] >= maxLifetime && (deleteOnlyWhenCulled ? !hl.renderer.isVisible : true))
    //                {
    //                    ShaderMoveArrayElements(i + 1, hl.lifetimes.Count, hl.material, ref hl.values);
    //                    hl.lifetimes.RemoveAt(i);
    //                    i--;
    //                }
    //            }
    //        }
    //    }
    //}

    private bool HoleExists(Vector3 value, Holes hole)
    {
        for (int i = 0; i < hole.lifetimes.Count; i++)
        {
            if (Vector3.SqrMagnitude((Vector3)hole.values[i] - value) <= hole.values[i].w * hole.values[i].w + 2e-2f)
            {
                return true;
            }
        }
        return false;
    }

    // Array operations
    private void ShaderSetArray(Vector4[] values, Material mat)
    {
        for (int i = 0; i < Holes.maxHoles; i++)
        {
            mat.SetVector("holes" + i.ToString(), values[i]);
        }
    }
    private void ShaderSetArrayElement(Vector4 vec, int index, Material mat, ref Vector4[] values)
    {
        mat.SetVector("holes" + index.ToString(), vec);
        values[index] = vec;
    }
    private void ShaderMoveArrayElements(int start, int end, Material mat, ref Vector4[] values)
    {
        for (int i = start; i < end + 1; i++)
        {
            if (i < Holes.maxHoles)
            {
                mat.SetVector("holes" + (i - 1).ToString(), values[i]);
                values[i - 1] = values[i];
            }
            else
            {
                mat.SetVector("holes" + (i - 1).ToString(), Vector4.zero);
                values[i - 1] = Vector4.zero;
            }
        }
    }
}
