﻿using UnityEngine;
using System.Collections.Generic;

public class STBHMaterial : MonoBehaviour
{
    public Material material;
    public Collider[] associatedColliders = new Collider[1];
    public bool useLifetime;
    public float maxLifetime = 5;
    public bool deleteWhenCulled;

    // The holes, both in values and lifetimes, are sorted by created time order
    [HideInInspector]
    //Each Vector4 represents a Vector3 position + float size
    public Vector4[] values = new Vector4[STBHManager.maxHoles];
    [HideInInspector]
    public List<float> lifetimes = new List<float>();
    [HideInInspector]
    public GameObject[] decals = new GameObject[0];
    [HideInInspector]
    public GameObject[] particles = new GameObject[0];
    [HideInInspector]
    public bool global = false;
}
