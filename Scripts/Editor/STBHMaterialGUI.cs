using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(STBHMaterial))]
public class STBHMaterialGUI : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
	}
}
