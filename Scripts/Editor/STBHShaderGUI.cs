﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
// GUI dos materiais que implementam o STBH
public class STBHShaderGUI : ShaderGUI
{
    // Mostrar opções?
    bool showOptions = true;
    // Mostrar preferências das distorções?
    bool showDistortion = false;
    
    // Função para desenhar Keywords da shader como uma opção booleana no inspector.
    private bool DrawKeywordBool (Material target, string name, string keyword)
    {
        bool active = Array.IndexOf(target.shaderKeywords, keyword) != -1;
        EditorGUI.BeginChangeCheck();
        active = EditorGUILayout.Toggle(name, active);
        if (EditorGUI.EndChangeCheck())
        {
            // enable or disable the keyword based on checkbox
            if (active)
                target.EnableKeyword(keyword);
            else
                target.DisableKeyword(keyword);
        }
        return active;
    }

    // Função para desenhar uma lista de objetos no inspector.
    private List<GameObject> DrawGOList (List<GameObject> list)
    {
        EditorGUILayout.BeginVertical();
        for (int i = 0; i < list.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();

            list[i] = EditorGUILayout.ObjectField(list[i], typeof(GameObject), false) as GameObject;

            if (GUILayout.Button("-", GUILayout.MaxWidth(16), GUILayout.MaxHeight(16)))
            {
                list.RemoveAt(i);
                i--;
            }

            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();

        if (GUILayout.Button("Add Decal", /*GUILayout.MaxWidth(18), */GUILayout.MaxHeight(16)))
        {
            list.Add(null);
        }

        return list;
    }
    
    // Loop de renderização da GUI.
    public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties)
    {
        Material targetMat = materialEditor.target as Material;

        // Opções
        if (showOptions = EditorGUILayout.Foldout(showOptions, "STBH Options"))
        {
            EditorGUI.indentLevel++;
            #region Espaço
            bool global = DrawKeywordBool(targetMat, "Global Space", "GLOBAL_SPACE");
            if (!global)
                EditorGUILayout.HelpBox("If objects that use this material will suffer batching (e.g. static objects with static batching), you should use global space.", MessageType.Warning);
            else
                EditorGUILayout.HelpBox("If objects that use this material will move, uncheck this option.", MessageType.Warning);
            #endregion
            #region Distorção
            bool distortion = DrawKeywordBool(targetMat, "Distortion", "DISTORTIONTXT");
            if (distortion)
            {
                EditorGUI.indentLevel++;
                if (showDistortion = EditorGUILayout.Foldout(showDistortion, "parameters"))
                {
                    Texture2D distTxt = targetMat.GetTexture("_Noise") as Texture2D;
                    EditorGUI.BeginChangeCheck();
                    EditorGUILayout.BeginHorizontal();
                    distTxt = (Texture2D)EditorGUILayout.ObjectField(distTxt, typeof(Texture2D), false);
                    if (EditorGUI.EndChangeCheck())
                    {
                        targetMat.SetTexture("_Noise", distTxt);
                    }

                    float strength = targetMat.GetFloat("_DistAmount");
                    EditorGUI.BeginChangeCheck();
                    strength = EditorGUILayout.Slider("strength", strength, 0, 2f);
                    if (EditorGUI.EndChangeCheck())
                    {
                        targetMat.SetFloat("_DistAmount", strength);
                    }
                    EditorGUILayout.EndHorizontal();
                    Vector2 size = targetMat.GetTextureScale("_Noise");
                    EditorGUI.BeginChangeCheck();
                    size = EditorGUILayout.Vector2Field("Size", size);
                    if(EditorGUI.EndChangeCheck())
                    {
                        targetMat.SetTextureScale("_Noise", size);
                    }
                }
                EditorGUI.indentLevel--;
                EditorGUILayout.Space();
            }
            #endregion
            #region Contorno
            EditorGUILayout.BeginHorizontal();
            bool outline = DrawKeywordBool(targetMat, "Outline", "OUTLINE");
            if (outline)
            {
                float size = targetMat.GetFloat("_outlineSize");
                EditorGUI.BeginChangeCheck();
                size = EditorGUILayout.Slider("Size", size, 1, 2);
                if (EditorGUI.EndChangeCheck())
                {
                    targetMat.SetFloat("_outlineSize", size);
                }
            }
            EditorGUILayout.EndHorizontal();
            #endregion

            EditorGUI.indentLevel--;
        }
        EditorGUILayout.Space();
        base.OnGUI(materialEditor, properties);
    }
}
