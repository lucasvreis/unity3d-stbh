﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class Holes {
    // Maximum number of holes, remember to set it in the shader too!
    public const int maxHoles = 16;

    // The holes, both in values and lifetimes, are sorted by created time order
    public Vector4[] values = new Vector4[maxHoles]; //Each Vector4 represents a Vector3 position + float size
    public List<float> lifetimes = new List<float>();
    public GameObject[] decals = new GameObject[0];
    public GameObject[] particles = new GameObject[0];
    public Material material;
    public Renderer renderer;
    public bool global;
    public UnityEvent<float> onAddHole;

    public Holes(Material material, Renderer renderer, bool global)
    {
        this.material = material;
        this.renderer = renderer;
        this.global = global;
    }
}
