# **See-through Bullet Holes for Unity3D** #

Unity3D package for creating transparent holes in meshes.

See Downloads for .unitypackage files.