﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "STBH/STBHStandard" {
	Properties {
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_BumpMap("Normalmap", 2D) = "bump" {}

		[HideInInspector]_outlineSize("", Float) = 1.3
		[HideInInspector]_Noise("Distortion Texture", 2D) = "white" {}
		[HideInInspector]_DistAmount("Distortion Amount", Range(0,0.999)) = 0.5

	}
	SubShader {
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		#pragma shader_feature GLOBAL_SPACE
		#pragma shader_feature OUTLINE
		#pragma shader_feature DISTORTIONTXT

		#pragma surface surf Standard addshadow vertex:vert

		#pragma target 3.0

		// Máximo de buracos, modificar também o script ShaderHoleManager
		static const int MaxHoles = 16;
		
		// Texturas
		sampler2D _MainTex;
		sampler2D _BumpMap;

		
		// Configurações
		half _Glossiness;
		half _Metallic;

		// Cores
		half4 _Color;

		// Array para armazenar as coordenadas e tamanho dos buracos (por meio do script)
		half4 holes[MaxHoles];

#if DISTORTIONTXT
		sampler2D _Noise;
		half _DistAmount;
#endif
#if OUTLINE
		// Espessura do contorno
		half _outlineSize;
#endif

#ifdef SHADER_API_D3D11
		// DX11-specific code
		StructuredBuffer<float4> tst;
#endif

		struct Input {
			float2 uv_MainTex;
			float2 uv_BumpMap;
#if DISTORTIONTXT
			float2 uv_Noise;
#endif
			half3 pos;
		};

		// Comprimento do vetor ao quadrado (mais rápido que calcular a raiz)
		half sqrLength(half3 vec)
		{
			return (vec.x*vec.x) + (vec.y*vec.y) + (vec.z*vec.z);
		}

		fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten)
		{
			fixed4 c;
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			return c;
		}

		void vert(inout appdata_full v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
#if !GLOBAL_SPACE
			o.pos = v.vertex;
#else
			o.pos = mul(unity_ObjectToWorld, v.vertex).xyz;
#endif
		}

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			half4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			half3 n = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));

			for (int i = 0; i < MaxHoles; i++)
			{
				if (all(holes[i] == half4(0, 0, 0, 0)))
					break;

				half sqrDist = sqrLength(IN.pos - holes[i].xyz);
#if DISTORTIONTXT
				sqrDist *= 1 - (tex2D(_Noise, IN.uv_Noise)*_DistAmount);
#endif

				half sqr = holes[i].a*holes[i].a;

				if (sqrDist < sqr)
				{
					discard;
					break;
				}
#if OUTLINE
				else if (sqrDist < sqr*_outlineSize)
				{
					half3 normal = normalize(IN.pos - holes[i].xyz);
					n = lerp(normal, n, 0.5);
				}
#endif
			}
			o.Albedo = c.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Normal = n;
		}
		ENDCG
	}
	FallBack "Diffuse"
	CustomEditor "STBHShaderGUI"
}
